

abstract type DC{N} end

struct ArithmeticC{G,R,N} <: DC{N}
    gin::G
    rin::R
    gout::G
    rout::R
    mode::Symbol
    in1::SignalSpec
    in2::SignalSpec
    out::SignalSpec
    name::Union{Symbol,Nothing}
end

function arithmeticC(n::N) where {N}
    a = ArithmeticC{green(N),red(N),N}(
        green(n), red(n), green(n), red(n),
        :*, EmptySignal(), EmptySignal(), EmptySignal(), nothing)
    registerDC(n, a)
    return a
end

function arithmeticC(n::N, out::SignalSpec, mode::Symbol, in1::SignalSpec, in2::SignalSpec; name = nothing) where {N}
    a = ArithmeticC{green(N),red(N),N}(
        green(n), red(n), green(n), red(n),
        mode, out, in1, in2, name)
    registerDC(n, a)
    return a
end

function convert_combinator(::Type{N2}, a::ArithmeticC{G,R,N}, gm, rm) where {N2,G,R,N}
    GNew = green(N2)
    RNew = red(N2)
    return ArithmeticC{GNew,RNew,N2}(
        gm(a.gin, :gin), rm(a.rin, :rin), gm(a.gout, :gout), rm(a.rout, :rout),
        a.mode, a.in1, a.in2, a.out, a.name)
end

