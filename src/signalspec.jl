
abstract type SignalSpec end

struct EmptySignal <: SignalSpec end

struct Virtual <: SignalSpec
    name::Symbol
end

struct Item <: SignalSpec
    name::Symbol
end

struct Fluid <: SignalSpec
    name::Symbol
end

name_string(::EmptySignal) = ""
name_string(s::Virtual) = "signal-" * string(s.name)
name_string(s::SignalSpec) = string(s.name)

type_string(::Virtual) = "virtual"
type_string(::Item) = "item"
type_string(::EmptySignal) = "virtual"
type_string(::Fluid) = "fluid"

