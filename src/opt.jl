
using JuMP
using HiGHS

struct CN
    s::Set{Int}
    CN(is...) = new(Set(is))
end

@kwdef struct ProbDesc
    cns::Vector{CN}
    n::Int
    max_dist_x::Int
    max_dist_y::Int
    M::Int
    optimise::Bool
end

function Base.show(io::IO, m::MIME"text/plain", p::ProbDesc)
    networks = mapreduce(x -> "\t" * string(x.s |> collect), (a, b) -> a * "\n" * b, p.cns)
    s = """
    Problem Desc:
        Networks:
    $networks 
        n: $(p.n)
        max_dist_x: $(p.max_dist_x)
        max_dist_y: $(p.max_dist_y)
        M: $(p.M)
        optimise: $(p.optimise)"""
    print(s)
end

function cnids(p::ProbDesc)
    1:length(p.cns)
end

function generate_problem(desc::ProbDesc)
    m = Model(HiGHS.Optimizer)
    n = desc.n
    M = desc.M
    M2 = round(M // 2)
    n_cns = length(desc.cns)
    @variable(m, x[1:n], Int)
    @variable(m, y[1:n], Int)
    @variable(m, xy_d[1:n, 1:M, 1:M], Bin)
    @variable(m, E[1:n, 1:n, 1:n_cns], Bin)

    @variable(m, dx[1:n, 1:n])
    @variable(m, dy[1:n, 1:n])

    @constraint(m, x[1] == round(M // 2))
    @constraint(m, y[1] == round(M // 2))

    for posx in 1:M, posy in 1:M
        @constraint(m, sum(xy_d[:, posx, posy]) <= 1)
    end

    for i in 1:n
        @constraint(m, sum(xy_d[i, :, :]) == 1)
    end

    for i in 1:n
        @constraint(m, sum((posx - 1) * sum(xy_d[i, posx, :]) for posx = 1:M) == x[i])
        @constraint(m, sum((posy - 1) * sum(xy_d[i, :, posy]) for posy = 1:M) == y[i])
    end

    if (desc.optimise)
        @variable(m, target)
        for i in 1:n
            @constraint(m, target >= x[i] - M2)
            @constraint(m, target >= -x[i] + M2)
            @constraint(m, target >= y[i] - M2)
            @constraint(m, target >= -y[i] + M2)
        end
        @objective(m, Min, target)
    end

    for i in 1:n, j in i:n
        @constraint(m, dx[i, j] == dx[j, i])
    end
    for cnid in cnids(desc), i in 1:n, j in i:n
        @constraint(m, E[i, j, cnid] == E[j, i, cnid])
    end

    for i in 1:n, j in 1:n
        @constraint(m, dx[i, j] >= x[i] - x[j])
        @constraint(m, dx[i, j] >= x[j] - x[i])
        @constraint(m, dy[i, j] >= y[i] - y[j])
        @constraint(m, dy[i, j] >= y[j] - y[i])
    end

    for cnid in cnids(desc)
        cn = desc.cns[cnid]
        for i in cn.s, j in cn.s
            @constraint(m, dx[i, j] <= desc.max_dist_x + desc.M * (1 - E[i, j, cnid]))
            @constraint(m, dy[i, j] <= desc.max_dist_y + desc.M * (1 - E[i, j, cnid]))
        end
    end

    for cnid in cnids(desc)
        cn = desc.cns[cnid]
        f = @variable(m, [cn.s, cn.s, cn.s], base_name = "f$cnid")
        r = minimum(cn.s)
        for k in cn.s
            k == r && continue
            @constraint(m, sum(f[r, :, k]) == 1)
            @constraint(m, sum(f[k, :, k]) == -1)
            for i in cn.s
                i == r && continue
                i == k && continue
                @constraint(m, sum(f[i, :, k]) == 0)
            end
        end
        for i in cn.s, j in cn.s, k in cn.s
            i >= j && continue
            @constraint(m, f[i, j, k] == -f[j, i, k])
        end
        for i in cn.s, j in cn.s, k in cn.s
            @constraint(m, f[i, j, k] <= E[i, j, cnid])
            @constraint(m, f[j, i, k] <= E[i, j, cnid])
        end
        for i in cn.s, k in cn.s
            @constraint(m, f[i, i, k] == 0)
        end
    end

    m
end

function generate_prob_desc(n::SerialisedNetwork, ldesc::LayoutDesc, optimise)
    ncomb = length(n.combinators)
    cns = map(x -> CN(map(p -> first(p), x |> collect)...), n.connections)
    filter!(cn -> length(cn.s) > 1, cns)
    ProbDesc(cns=cns,
        n=ncomb,
        max_dist_x=ldesc.max_dist_x,
        max_dist_y=ldesc.max_dist_y,
        M=ldesc.M,
        optimise=optimise)
end

