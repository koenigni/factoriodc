module DCNS

include("layout_desc.jl")
include("signalspec.jl")
include("combinators.jl")
include("network.jl")
include("opt.jl")
include("layout.jl")
include("factorio_bp.jl")

function compile(n::Network, optimise=true, size=15)
	sn = serialise(n)
	l = layout(sn, LayoutDesc(7, 4, size), optimise)
	create_factorio_blueprint_string(l)
end

export compile
export @net
export Network
export green, red
export arithmeticC


end # module DCNS
