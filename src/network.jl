
using Accessors
using Graphs
using DataStructures

@enum WireType begin
    Green
    Red
end

abstract type IntermediateNetwork end

mutable struct Network <: IntermediateNetwork
    circuit_alloc::Int
    combinators::Vector{DC{Network}}
    circuit_equiv::Set{Pair{Int,Int}}

    Network() = new(1, [], Set())
end

struct Connection{Colour}
    id::Int
end

function connect(n::Network, cs::Vararg{Connection{Colour}}) where {Colour}
    c1 = first(cs)
    for c in cs[2:end]
        push!(n.circuit_equiv, c1.id => c.id)
    end
    n
end

function get_connection(::Val{WT}, n::Network) where {WT}
    id = n.circuit_alloc
    n.circuit_alloc += 1
    push!(n.circuit_equiv, id => id)
    return Connection{WT}(id)
end

green(n::Network) = get_connection(Val{Green}(), n)
red(n::Network) = get_connection(Val{Red}(), n)

green(::Type{Network}) = Connection{Green}
red(::Type{Network}) = Connection{Red}

function registerDC(n::Network, dc::DC)
    push!(n.combinators, dc)
    dc
end

function adjust(n, e)
    if e.args[1] == :green && length(e.args) == 1
        return :(green($n))
    end
    if e.args[1] == :red && length(e.args) == 1
        return :(red($n))
    end
    if e.args[1] == :connect
        args = recurse.(Ref(n), e.args[2:end])
        return :(connect($n, $(args...)))
    end
    if e.args[1] == :arithmeticC
        args = recurse.(Ref(n), e.args[2:end])
        return :(arithmeticC($n, $(args...)))
    end
    return Expr(:call, recurse.(Ref(n), e.args)...)
end

function recurse(n, e::Expr)
    if e.head == :call
        return adjust(n, e)
    else
        return Expr(e.head, recurse.(Ref(n), e.args)...)
    end
end

recurse(_, e) = e

macro net(n, block)
    return quote
        network = $(esc(n))
        $(recurse(:network, block))
        network
    end
end

struct SerialisedNetwork <: IntermediateNetwork
    combinators::Vector{DC{SerialisedNetwork}}
    connections::Vector{Set{Pair{Int,Symbol}}}
end

red(::Type{SerialisedNetwork}) = Int
green(::Type{SerialisedNetwork}) = Int

function serialise(n::Network)
    graph = SimpleGraph(n.circuit_equiv |> collect .|> Edge)
    comps = map(x -> Set(x), connected_components(graph))
    inverse_mapping = Dict{Int,Int}()
    for (i, comp) in enumerate(comps)
        for n in comp
            push!(inverse_mapping, n => i)
        end
    end
    connections = map(x -> Set{Pair{Int,Symbol}}(), comps)
    combinators = Vector{DC{SerialisedNetwork}}()
    for (id, dc) in enumerate(n.combinators)
        function m(conn, name)
            conn_comp_id = inverse_mapping[conn.id]
            push!(connections[conn_comp_id], id => name)
            conn_comp_id
        end
        new_dc = convert_combinator(SerialisedNetwork, dc, m, m)
        push!(combinators, new_dc)
    end

    return SerialisedNetwork(combinators, connections)
end

struct LayoutedNetwork <: IntermediateNetwork
    combinators::Vector{DC{LayoutedNetwork}}
    combinator_pos::Vector{Pair{Int,Int}}
end

green(::Type{LayoutedNetwork}) = Pair{Int,Set{Pair{Int,Symbol}}}
red(::Type{LayoutedNetwork}) = Pair{Int,Set{Pair{Int,Symbol}}}

function Base.show(io::IO, m::MIME"text/plain", p::LayoutedNetwork)
    remove_empty(x) = length(x) == 0 ? "()" : x
    s = ""
    for (dc, pos, i) in zip(p.combinators, p.combinator_pos, eachindex(p.combinators))
        s *= "Combinator $i ($(first(pos)), $(last(pos)))\n"
        s *= "  gin: $(last(dc.gin) |> remove_empty) ($(first(dc.gin)))\n"
        s *= "  rin: $(last(dc.rin) |> remove_empty) ($(first(dc.rin)))\n"
        s *= "  gout: $(last(dc.gout) |> remove_empty) ($(first(dc.gout)))\n"
        s *= "  rout: $(last(dc.rout) |> remove_empty) ($(first(dc.rout)))\n"
    end
    print(s)
end

