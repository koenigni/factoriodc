
coord(ipres, i, dir) = round(Int, value(ipres[dir][i]))

is_reachable(c1, c2, desc::LayoutDesc) =
    let p1 = first(c1), p2 = first(c2),
        dx = abs(first(p1) - first(p2)), dy = abs(last(p1) - last(p2))

        dx <= desc.max_dist_x && dy <= desc.max_dist_y
    end

get_colour(s::Symbol) = s == :gin || s == :gout ? Green : Red

adjust_connid(n::SerialisedNetwork, dcid) = function (cid, name)
    conns = copy(n.connections[cid])
    conns = filter(x -> (x != (dcid => name)), conns)
    cid => conns
end

function final_layout(n::SerialisedNetwork, ipres, _::LayoutDesc)
    combinator_pos = map(i -> coord(ipres, i, :x) => coord(ipres, i, :y),
        eachindex(n.combinators))
    combinators = map(dcid -> convert_combinator(LayoutedNetwork, n.combinators[dcid],
            adjust_connid(n, dcid), adjust_connid(n, dcid)),
        eachindex(n.combinators))
    return LayoutedNetwork(combinators, combinator_pos)
end

function layout(n::SerialisedNetwork, desc::LayoutDesc, optimise=false)
    prob_desc = generate_prob_desc(n, desc, optimise)
    prob = generate_problem(prob_desc)
    optimize!(prob)
    final_layout(n, prob, desc)
end
