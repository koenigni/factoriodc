using JSON
using CodecZlib
using Base64

function create_factorio_blueprint_string(l::LayoutedNetwork)
    # Create the blueprint JSON
    blueprint_dict = create_blueprint_dict(l)

    # Convert the dictionary to a JSON string
    json_string = JSON.json(blueprint_dict)

    return json_to_factorio(json_string)
end

function json_to_factorio(json_string::String)
    # Compress the JSON string using zlib
    compressed_data = transcode(ZlibCompressor, json_string)

    # Base64 encode the compressed data
    encoded_string = base64encode(compressed_data)

    # Prepend the version number (currently 0)
    return "0" * encoded_string

end

function create_blueprint_dict(l::LayoutedNetwork)
    entities = []
    for (index, combinator) in enumerate(l.combinators)
        x, y = l.combinator_pos[index].first, l.combinator_pos[index].second
        entity = Dict(
            "entity_number" => index,
            "name" => "arithmetic-combinator",
            "position" => Dict("x" => x * 2 - 0.5, "y" => y * 2 - 0.5),
            "direction" => 2,  # Assuming default direction, adjust if needed
            "control_behavior" => Dict(
                "arithmetic_conditions" => Dict(
                    "first_signal" => signal_to_dict(combinator.in1),
                    "second_signal" => signal_to_dict(combinator.in2),
                    "output_signal" => signal_to_dict(combinator.out),
                    "operation" => arithmetic_mode_to_string(combinator.mode)
                )
            ),
            "connections" => create_connections(combinator, index)
        )

        push!(entities, entity)
    end

    blueprint = Dict(
        "blueprint" => Dict(
            "icons" => [Dict(
                "signal" => Dict(
                    "type" => "item",
                    "name" => "arithmetic-combinator"
                ),
                "index" => 1
            )],
            "entities" => entities,
            "item" => "blueprint",
            "version" => 281479275151360  # Current Factorio version, update as needed
        )
    )

    return blueprint
end

function signal_to_dict(signal::SignalSpec)
    return Dict("type" => type_string(signal), "name" => name_string(signal))
end

function arithmetic_mode_to_string(mode)
    return string(mode)
end

to_circuit_id(name) = name == :gin || name == :rin ? 1 : 2

function create_connections(combinator, index)
    connections = Dict()

    if !isempty(combinator.gin.second)
        connections["1"] = Dict("green" => [
            Dict("entity_id" => conn.first,
                "circuit_id" => to_circuit_id(conn.second))
            for conn in combinator.gin.second])
    end
    if !isempty(combinator.rin.second)
        connections["1"] = get(connections, "1", Dict())
        connections["1"]["red"] = [
            Dict("entity_id" => conn.first,
                "circuit_id" => to_circuit_id(conn.second))
            for conn in combinator.rin.second]
    end
    if !isempty(combinator.gout.second)
        connections["2"] = Dict("green" => [
            Dict("entity_id" => conn.first,
                "circuit_id" => to_circuit_id(conn.second))
            for conn in combinator.gout.second])
    end
    if !isempty(combinator.rout.second)
        connections["2"] = get(connections, "2", Dict())
        connections["2"]["red"] = [
            Dict("entity_id" => conn.first,
                "circuit_id" => to_circuit_id(conn.second))
            for conn in combinator.rout.second]
    end

    return connections
end

