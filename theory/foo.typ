
$&X_i in ZZ "x pos of DC" i\
&Y_i in ZZ "y pos of DC" i\
&"dx"_(i j) >= X_i - X_j\ 
&"dx"_(i j) >= X_j - X_i\
&"dy"_(i j) >= Y_i - Y_j\ 
&"dy"_(i j) >= Y_j - Y_i\
forall N in "CNs" forall e in N:& "dx"_e <= "max dist"_x + M (1 - E^N_e)\ // cries in big m constraints
forall N in "CNs"forall e in N:& "dy"_e <= "max dist"_y + M (1 - E^N_e)\ // cries in big m constraints
& E^N_(i j) in {0, 1} = 1 "if connection in CN" N "between" i "and" j, "else" 0\
& f^N_(i j,k) in RR_(>= 0) "the flow on the edge" i j "destined for" k "in the CN" N\
forall N in "CNs" forall k in N \\ {r}: & 
  limits(sum)_(j in N\\{r}) f^N_(r j, k) = 1 \ 
  & limits(sum)_(j in N\\{k}) f^N_(j k, k) = 1 \
  & forall i in N \\ {r, k} : limits(sum)_(j in N\\{i}) f^N_(j i, k) = 0 \
forall N in "CNs" forall i,j,k in N: & f^N_(i j, k) = -f^N_(j i, k) \
& f^N_(i j, k) <= E^N_(i j)
$






